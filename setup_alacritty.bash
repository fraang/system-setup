#!/usr/bin/env bash

CARGO_DIR="${HOME}/.cargo/"

sudo install -s -Dm755 "${CARGO_DIR}bin"
