#!/usr/bin/env bash

if ! command -v feh &> /dev/null; then
    sudo apt install feh
fi

feh --bg-max "${HOME}/.local/share/fraang/background/iss_by_night.png"

