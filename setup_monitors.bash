#!/usr/bin/env bash

xrandr --output eDP-1-1 --auto
xrandr --output DP-1-1-1 --auto --right-of eDP-1-1
xrandr --output DP-1-1-3 --auto --right-of DP-1-1-1 --rotate left

